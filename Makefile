VERSION=34.0.0
SDK_TOOLS=/usr/lib/android-sdk/
JAVAC=$(shell which javac)
APP=exploit
DX_IS_PRESENT=$(shell test -f $(SDK_TOOLS)/build-tools/$(VERSION)/dx; echo $$?)

VERSION_B=$(basename $(basename $(VERSION)))

pass=ahahah

all: $(shell mkdir -p build)
all: clean res build/$(APP).apk
signature_v1: clean res build/$(APP).v1.apk

build/%.v1signed.apk: ./build/%.unsigned.apk ./ToyKey.keystore
	jarsigner -verbose -keystore ./ToyKey.keystore -storepass $(pass) -keypass $(pass) -signedjar $@ $< MadKey

build/%.v1.apk: ./build/%.v1signed.apk
	$(SDK_TOOLS)/build-tools/$(VERSION)/zipalign -v -f 4 $< $@

res:
	$(SDK_TOOLS)/build-tools/$(VERSION)/aapt package -v -f -m  -S ./res -J ./src -M ./AndroidManifest.xml -I "$(SDK_TOOLS)/platforms/android-$(VERSION_B)/android.jar"

class: $(shell find java -type f -regex ".*\.java")
	$(JAVAC) -d ./build -classpath $(SDK_TOOLS)/platforms/android-$(VERSION_B)/android.jar -sourcepath ./java $^

build/classes.dex: class $(shell find build -type f -regex ".*\.class")
ifeq ($(DX_IS_PRESENT), 0)
	$(SDK_TOOLS)/build-tools/$(VERSION)/dx --dex --verbose --output=$@ ./build
else
	$(SDK_TOOLS)/build-tools/$(VERSION)/d8 --classpath $(SDK_TOOLS)/platforms/android-$(VERSION_B)/android.jar $(shell find build -type f -regex ".*\.class" -printf "'%p'\n") --output build
endif

build/%.unsigned.apk: build/classes.dex
	$(SDK_TOOLS)/build-tools/$(VERSION)/aapt package -v -f -M ./AndroidManifest.xml -S ./res -I $(SDK_TOOLS)/platforms/android-$(VERSION_B)/android.jar -F $@ ./build

build/%.v2aligned.apk: ./build/%.unsigned.apk ./ToyKey.keystore
	$(SDK_TOOLS)/build-tools/$(VERSION)/zipalign -v -f 4 $< $@

build/%.apk: ./build/%.v2aligned.apk
	$(SDK_TOOLS)/build-tools/$(VERSION)/apksigner sign -ks ./ToyKey.keystore --v2-signing-enabled true --in $< --out $@ --ks-pass pass:$(pass)


ToyKey.keystore :
	keytool -genkeypair -validity 1000 -dname "CN=MadSquirrel,O=Android,C=FR" -keystore $@ -storepass $(pass) -keypass $(pass) -alias MadKey -keyalg RSA -v

clean:
	$(RM) -r build/*

clean_all: clean
	$(RM) ToyKey.keystore
